## Hello, World! 🌎🌍🌏

As a self-taught software developer, I took my education further by attending [42](https://42.fr/en/), a vocational school for software developers. I then spent 3 years working independently as full-stack web-developer for several clients. Most recently I've attended my first batch at [The Recurse Center](https://recurse.com), an educational retreat for programmers, where I learned about Rust and Embedded Systems!

- 🌱 **Currently learning:** Rust
- 🛠️ **Working on:** 
  - [fractol-rs](https://github.com/ChrisRenfrow/fractol-rs) (Rust) - a rewriting of an old C program I made to explore fractals 
  - [gol-lib](https://github.com/ChrisRenfrow/gol-lib) (Rust) - a game of life library
  - [gol-midi](https://github.com/ChrisRenfrow/gol-midi) (Rust) - a program that translates a game of life board-state as a MIDI controller
- 💬 **Talk to me about:** Emacs, Nix, Rust
- 🐘 **Follow me on:** <a rel="me" href="https://recurse.social/@crenfrow">Mastodon</a>
